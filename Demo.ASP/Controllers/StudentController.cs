﻿using DemoADO.DAL.Models;
using DemoADO.DAL.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Demo.ASP.Controllers
{
    public class StudentController : Controller
    {
        // GET: Student
        public ActionResult Index()
        {
            StudentService serv = new StudentService();
            List<Student> model = serv.GetAll(); 
            return View(model);
        }

        public ActionResult Details(int id)
        {
            StudentService serv = new StudentService();
            Student model = serv.Get(id);
            return View(model);
        }
    }
}