﻿
using DemoADO.DAL.Models;
using DemoADO.DAL.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoADO
{
    class Program
    {
        static void Main(string[] args)
        {
            StudentService service = new StudentService();
            List<Student> list = service.GetAll();
            foreach(Student s in list)
            {
                Console.WriteLine(s.FirstName);
            }
            Console.ReadKey();
        }
    }
}
