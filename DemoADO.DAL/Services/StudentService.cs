﻿using DemoADO.DAL.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoADO.DAL.Services
{
    public class StudentService
    {
        private string connectionString = @"Data Source=DESKTOP-HKVTM9G\SQLSERVER;Initial Catalog=Demo;UID=sa;Pwd=test1234=";
       public List<Student> GetAll()
        {
            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();
            SqlCommand cmd = connection.CreateCommand();
            cmd.CommandText = "SELECT * FROM student";
            SqlDataReader r = cmd.ExecuteReader();
            List<Student> result = new List<Student>();
            while(r.Read())
            {
                result.Add(new Student
                {
                    Id = (int)r["student_id"],
                    LastName = (string)r["last_name"],
                    FirstName = (string)r["first_name"],
                    sectionId = (int)r["section_id"]
                });
            }
            connection.Close();
            return result;
        }

        public Student Get(int id)
        {
            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();
            SqlCommand cmd = connection.CreateCommand();
            cmd.CommandText = "SELECT * FROM student WHERE student_id = " + id;
            SqlDataReader r = cmd.ExecuteReader();
            Student s = null;
            if (r.Read())
            {
                s = new Student
                {
                    Id = (int)r["student_id"],
                    LastName = (string)r["last_name"],
                    FirstName = (string)r["first_name"],
                    sectionId = (int)r["section_id"]
                };
            }
            connection.Close();
            return s;
        }

        //public int Insert(Student s)
        //{

        //}
    }
}
